# EMOTION CLASSIFICATION SERVICE

ML service for classifying emotions in text using 
* FastAPI 
* Celery 
* RabbitMQ
* Redis

### How to reproduce this project?

Clone this repo into your local machine with the command:

```git clone https://gitlab.com/avisprof/service_fastapi_celery.git```

Model can be deployed locally using Docker and the following command:

```docker compose up --build```

Web-server is available by URL:

```http://localhost:8000```

Swagger UI is available by URL:

```http://localhost:8000/docs```

![01_swagger.png](/reports/01_swagger.png)

