FROM python:3.9.19-slim

WORKDIR /app

COPY pyproject.toml poetry.lock /app/

RUN pip install --no-cache-dir poetry=="1.8.3"

RUN poetry install --only main,worker

COPY . /app

CMD ["poetry", "run", "celery", "-A", "src.worker.predict", "worker", "--loglevel", "info"]

