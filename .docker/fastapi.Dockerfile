FROM python:3.9.19-slim

WORKDIR /app

COPY pyproject.toml poetry.lock /app/

RUN pip install --no-cache-dir poetry=="1.8.3"

RUN poetry install --only main,fastapi

COPY . /app

CMD ["poetry", "run", "uvicorn", "src.fast_app:app", "--host", "0.0.0.0", "--port", "8000"]
