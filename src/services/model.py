from transformers import pipeline
from loguru import logger

import torch
torch.set_num_threads(1) # устанавливаем количество потоков

class ModelName:

    model_name = 'seara/rubert-tiny2-ru-go-emotions'

    logger.info(f"start initialize model {model_name}...")
    model =  pipeline(model=model_name)
    logger.info(f"model {model_name} has been initialized")

    @classmethod
    def predict_smth(cls, text: str):

        logger.info(f"start model with text {text}")
        result = cls.model(text)
        logger.info(f"result of model: {result}")

        return result




    