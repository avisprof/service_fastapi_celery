from fastapi import APIRouter
from loguru import logger

from src.connections.broker import celery_app
from src.api.schemas import TaskRequest, TaskResult

router = APIRouter()

@router.get("/")
def check():
    return {'status':'ok'}

@router.post("/predict", response_model=TaskResult)
async def predict_smth(task_data: TaskRequest):

    logger.info(f"send task from router.py: {task_data.text}")

    async_result = celery_app.send_task("predict", args=[task_data.text])
    result = async_result.get()

    logger.info(f"get result from router.py: {result}")

    return {'status': 'ok',
            'result_label': result[0]['label'],
            'result_score': result[0]['score']}
