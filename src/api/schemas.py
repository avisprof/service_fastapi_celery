from pydantic import BaseModel, ConfigDict


class TaskRequest(BaseModel):
    """
    Модель запроса для классификации эмоций по тексту.

    Attributes:
        text (str): Текст для анализа эмоций.
    """
    text: str
    

    model_config = ConfigDict(
        json_schema_extra={"example": 
                           {'text': 'Привет! Как дела? Этот день просто замечательный!'}
    })


class TaskResult(BaseModel):
    status: str
    result_label: str
    result_score: float



