from fastapi import FastAPI
from src.api.router import router
from loguru import logger

app = FastAPI()
app.include_router(router=router, prefix='/api')
logger.info("FastAPI application has been initialized")