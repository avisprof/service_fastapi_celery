from celery import Celery

celery_app = Celery("tasks",
                backend="redis://redis",
                broker="pyamqp://guest:guest@rabbitmq//")