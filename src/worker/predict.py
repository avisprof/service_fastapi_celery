from src.connections.broker import celery_app
from src.services.model import ModelName
from loguru import logger

@celery_app.task(name="predict")
def predict_smth(text):

    try:
        result = ModelName.predict_smth(text)
        return result
    except Exception as ex:
        logger.error([ex])
        return [{'result_label':None, 'result_score': 0}]
